<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
// use Yajra\Oci8\Eloquent\OracleEloquent as Eloquent;

class AljamiaPayChallan extends Model
{
    protected $table = 'paychallan';
    protected $primaryKey = null;
    public $incrementing = false;
    public $timestamps = false;

    public function detail()
    {
    	return $this->hasMany('App\AljamiaPayChallanDetail', 'challanno', 'challanno');
    }

    public function user()
    {
    	return $this->belongsTo('App\AljamiaStudent', 'regno', 'regno');
    }
}
