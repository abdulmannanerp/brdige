<?php

namespace App\Http\Middleware;

use Closure;

class RevokeUnknownIps
{
    private $allowedIps = [
        '95.216.45.51', //iiu main server
        '111.68.97.170', //iiu ip address
        '135.181.17.222', //iiu ip address
        '192.168.10.1', //iiu ip address
		'172.67.128.47',
        '192.168.11.2',
        '37.27.224.27',
        '65.108.140.190',
    ];

    public function __construct()
    {
        if (app()->env == 'local') {
            $this->allowedIps[] = '127.0.0.1';
        }
    }
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (in_array(request()->ip(), $this->allowedIps)) {
            return $next($request);    
        }
        return redirect()->route('home');
    }
}
