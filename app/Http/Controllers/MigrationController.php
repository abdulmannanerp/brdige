<?php

namespace App\Http\Controllers;

use App\AljamiaGenderFix;
use App\AljamiaNewAcadRecord;
use App\AljamiaNewStudent;
use Illuminate\Http\Request;

class MigrationController extends Controller
{
    public function index()
    {
        return 'You\'ve landed on jupiter';
    }

    public function updateGender()
    {
        $content = json_decode(request()->getContent());
        $gender = ($content->gender == 'Male') ? 'M' : 'F';
        AljamiaGenderFix::create([
            'NIDPASSNO' => $content->cnic,
            'SEX' => $gender
        ]);
        return 'Done';
    }

    public function migrate()
    {
        $content = json_decode(request()->getContent());
        $gender = ($content->gender == 'Male') ? 'M' : 'F';
        $this->createStudentRecord($content, $gender);
        $education = $this->populateEducationArray($content);
        foreach ($education as $key => $value) {
            $degree = $value['degree'];
            $completionyear = ($value['completionyear']) ? date_format(date_create($value['completionyear']), 'Y') : '';
            $univboard = $value['univboard'];
            $majorsubjects = $value['majorsubjects'];
            $marksobt = $value['marksobt'];
            $marksTotal = $value['total'];
            AljamiaNewAcadRecord::updateOrCreate([
                'REGNO' => $content->cnic,
            ],[
                'REGNO' => $content->cnic,
                'DEGREE' => $degree,
                'YEAROFEXAM' => $completionyear,
                'UNIVBOARD' => $univboard,
                'MAJORSUBJECTS' => $majorsubjects,
                'MARKSOBT' => $marksobt,
                'TOTAL' => $marksTotal,
                'DIVISION' => '',
                'PERCENTAGE' => '',
                'BATCHCODE' => '',
                'ACADPROGCODE' => '',
                'DEPCODE' => '',
                'FACCODE' => '',
                'NIDPASSNO' => $content->cnic,
                'THESISSUPERVISER' => '',
                'APPROVAL_OF_SYNOP_DATE' => '',
                'GAT_TEST' => '',
                'COMPREHENSIVE_EXAM' => '',
                'LAST_RESEARCH_TITLE' => ''
            ]);
        }
    }

    public function createStudentRecord($content, $gender)
    {
        AljamiaNewStudent::updateOrCreate([
           'REGNO' => $content->cnic, 
        ],[
            'REGNO' => $content->cnic,
            'STUDNAME' => $content->name,
            'STUDFATHERNAME' => $content->fatherName,
            'NIDPASSNO' => $content->cnic,
            'STUDPRESADD' => $content->addressPresent,
            'STUDPERMADD' => $content->addressPermanent,
            'STUDEMAIL' => $content->email,
            'STUDPHONE' => $content->mobile,
            'BATCHCODE' => '',
            'STUDLOGIN' => '',
            'STUDPASS' => '',
            'NATIONALITY' => $content->nationality,
            'DATEOFBIRTH' => $content->dob,
            'CGPA' => '',
            'CREDHRATTEMPT' => '',
            'S_NO' => '',
            'STUDPOINTS' => '',
            'STATUS' => '',
            'STUDPRESADDONE' => $content->addressPresent,
            'STUDPRESADDTWO' => $content->addressPresent,
            'STUDPRESADDCITY' => $content->cityPresent,
            'STUDPRESADDZIP' => '',
            'STUDPRESADDCOUNTRY' => $content->country,
            'STUDPERMADDONE' => $content->addressPermanent,
            'STUDPERMADDTWO' => $content->addressPermanent,
            'STUDPERMADDCITY' => $content->cityPermanent,
            'STUDPERMADDZIP' => '',
            'STUDPERMADDCOUNTRY' => $content->country,
            'SEX' => $gender,
            'SECRETQUESTION' => '',
            'SECRETANSWER' => '',
            'ACADPROGCODE' => '',
            'DEPCODE' => '',
            'FACCODE' => '',
            'OVERALLPERCENTAGE' => '',
            'PROGCMPLTDATE' => '',
            'HIFZ' => '',
            'DEGREENO' => '',
            'THESISTITLE' => '',
            'READMIT' => '',
            'CONV' => '',
            'HIFZDATE' => '',
            'DISTRICT' => $content->district,
            'DOMICILE' => $content->domicile,
            'STUDSTATUS' => '',
            'STUDSTATUSDATE' => '',
            'SPECIALIZATION' => '',
            'REGNO_ALLOTED' => '',
            'REGNO_ALLOTED_DATE' => ''
        ]);
    }

    public function populateEducationArray($applicant)
    {
        $education = [];
        $education['ssc']['degree'] = 'SSC';
        $education['ssc']['univboard'] = $applicant->sscFrom;
        $education['ssc']['majorsubjects'] = $applicant->sscSubjects;
        $education['ssc']['marksobt'] = $applicant->sscObtainedMarks;
        $education['ssc']['total'] = $applicant->sscTotalMarks;
        $education['ssc']['completionyear'] = '';

        $hsscTotalMarks = '';
        $hsscObtainedMarsks = '';
        if ($applicant->hsscComposite) {
            $hsscTotalMarks = $applicant->hsscTotal;
            $hsscObtainedMarsks = $applicant->hsscComposite;
        } else {
            $hsscTotalMarks = (integer)$applicant->hssc1Total + (integer)$applicant->hssc2Total;
            $hsscObtainedMarsks = (integer)$applicant->hssc1 + (integer)$applicant->hssc2;
        }
        $education['hssc']['degree'] = 'HSSC';
        $education['hssc']['univboard'] = $applicant->hsscFrom;
        $education['hssc']['majorsubjects'] = $applicant->hsscSubjects;
        $education['hssc']['marksobt'] = $hsscObtainedMarsks;
        $education['hssc']['total'] = $hsscTotalMarks;
        $education['hssc']['completionyear'] = '';

        if ($applicant->baBscMarksObtained) {
            $education['baBsc']['degree'] = 'BA BSC';
            $education['baBsc']['univboard'] = $applicant->baBscFrom;
            $education['baBsc']['majorsubjects'] = $applicant->baBscSubjects;
            $education['baBsc']['marksobt'] = $applicant->baBscMarksObtained;
            $education['baBsc']['total'] = $applicant->baBscTotalMarks;
            $education['baBsc']['completionyear'] = $applicant->baBscDegreeCompletionYear;
        }

        if ($applicant->mscMarksObtained) {
            $education['baBsc']['degree'] = 'MSC';
            $education['baBsc']['univboard'] = $applicant->mscFrom;
            $education['baBsc']['majorsubjects'] = $applicant->mscSubjects;
            $education['baBsc']['marksobt'] = $applicant->mscMarksObtained;
            $education['baBsc']['total'] = $applicant->mscTotalMarks;
            $education['baBsc']['completionyear'] = $applicant->mscDegreeCompletionYear;
        }

        if ($applicant->bsMarksObtained) {
            $education['bs']['degree'] = 'BS';
            $education['bs']['univboard'] = $applicant->bsFrom;
            $education['bs']['majorsubjects'] = $applicant->bsSubjects;
            $education['bs']['marksobt'] = $applicant->bsMarksObtained;
            $education['bs']['total'] = $applicant->bsTotalMarks;
            $education['bs']['completionyear'] = $applicant->bsDegreeCompletionYear;
        }

        if ($applicant->msMarksObtained) {
            $education['bs']['degree'] = 'MS';
            $education['bs']['univboard'] = $applicant->msFrom;
            $education['bs']['majorsubjects'] = $applicant->msSubjects;
            $education['bs']['marksobt'] = $applicant->msMarksObtained;
            $education['bs']['total'] = $applicant->msTotalMarks;
            $education['bs']['completionyear'] = $applicant->msDegreeCompletionYear;
        }

        return $education;
    }
}
