<?php

namespace App\Http\Controllers;

use App\AljamiaStudent;
use DB;

class AljamiaStdPermissionController extends Controller
{
    /* ********************************************************************************************* */
    //                                     GET STUDENT PICTURE
    /* ********************************************************************************************* */
    public function getStudentPicture()
    {
        // return array("A","B");

        $request = json_decode(request()->getContent());
        if ($request && $request->regno) {
            $query = "select regno, STUDPIC from STUDENTPICTURELR
                         where regno = '" . $request->regno . "'";
            return DB::connection('oracle')->select($query);
        } else {
            return "Empty Picture Against Reg No";
        }

        return;
    }
    /* ********************************************************************************************* */
    //                                    GET STUDENT GPA/REMARKS/CrHrs
    /* ********************************************************************************************* */
    // public function getStudentGpaRemarksCrHrs(){
    //     $request = json_decode(request()->getContent());
    //     if ($request && $request->regno) {
    //         $query = "select distinct SEMESTER.SEMSTARTDATE, TBL_STUDSEM.semcode,
    //                     TBL_STUDSEM.UPTONOWPERCENTAGE, TBL_STUDSEM.UPTONOWCRDHRATT,
    //                     SUBSTR(TBL_STUDSEM.GPA, 1, INSTR(TBL_STUDSEM.GPA, '.')+2) RND_STUDSEM_GPA,
    //                     TBL_STUDSEM.REMARKS, SUBSTR(TBL_STUDSEM.SEMCGPA, 1, INSTR(TBL_STUDSEM.SEMCGPA, '.')+2) RND_STUDSEM_SEMCGPA
    //                     from tbl_studsemcourse ssc, TBL_STUDSEM, SEMESTER, TBL_STUDENT s
    //                     where ssc.regno = '".$request->regno."'
    //                     and TBL_STUDSEM.regno = ssc.regno
    //                     and TBL_STUDSEM.semcode = SEMESTER.semcode
    //                     order by SEMESTER.SEMSTARTDATE asc";
    //         return DB::connection('oracle') ->select($query);
    //     }else{
    //         return "Empty Record GPA and Remarks";
    //     }

    //     return;
    // }
    /* ********************************************************************************************* */
    //                                 GET STUDENT CGPA/Overall Percentage
    /* ********************************************************************************************* */
    // public function getStudentCgpaOverallPercentage()
    // {

    //     $request = json_decode(request()->getContent());
    //     if ($request && $request->regno) {
    //         $query = "select distinct SUBSTR(TBL_STUDENT.CGPA, 1, INSTR(TBL_STUDENT.CGPA, '.')+2) RND_STUDENT_CGPA,
    //                     round(TBL_STUDENT.OVERALLPERCENTAGE, 2)  RND_STUDENT_OVERALLPERCENTAGE
    //                     from TBL_STUDENT
    //                     where regno = '".$request->regno."'";
    //         return DB::select($query);
    //     }else{
    //         return "Empty Record Overall Percentage and CGPA";
    //     }

    //     return;
    // }
    /* ********************************************************************************************* */
    //                                     GET STUDENT COURSES
    /* ********************************************************************************************* */

    public function getstudentQrCodeData(){

        $request = json_decode(request()->getContent());
        if ($request && $request->regno) {
            
            $query = "select s.regno,facname, depname, acadprogname, batchname,studname, STUDENTIIUIEMAIL, studfathername, STUDPHONE, 
            CGPA, STUDEMAIL, STATUS, PROGCMPLTDATE
                    from TBL_FACULTY f, TBL_DEPARTMENT d, TBL_ACADPROG ap, 
                    tbl_batch b, TBL_STUDENT s
                    where s.regno = '" . $request->regno . "'
                    and s.faccode = f.faccode
                    and s.depcode = d.depcode
                    and s.acadprogcode = ap.acadprogcode
                    and s.batchcode = b.batchcode";


            return DB::select($query);

            
        }else {
            return "Empty Reg No";
        }

        return;

    }
    public function getStudentCourses()
    {

        $request = json_decode(request()->getContent());
        if ($request && $request->regno) {
            // $query="select ssc.regno, ssc.grade, ssc.coursecode, ssc.semcode, c.COURSENAME, c.CREDITHRS from TBL_STUDSEMCOURS ssc, semester s, tbl_course c where ssc.regno = '".$request->regno."' and ssc.semcode = s.semcode and ssc.depcode = c.depcode and ssc.coursecode = c.coursecode order by s.SEMSTARTDATE desc";
            // $query = "select ssc.regno, ssc.grade, ssc.coursecode, ssc.semcode, c.COURSENAME, c.CREDITHRS, facname, depname, acadprogname, batchname,
            //             studname, STUDENTIIUIEMAIL, studfathername, STUDPHONE, CGPA, STUDEMAIL from TBL_STUDSEMCOURS ssc, semester s, tbl_course c,
            //             TBL_FACULTY f, TBL_DEPARTMENT d, TBL_ACADPROG ap, tbl_batch b, TBL_STUDENT s
            //              where ssc.regno = '".$request->regno."'
            //                 and ssc.semcode = s.semcode
            //                 and ssc.depcode = c.depcode
            //                 and ssc.coursecode = c.coursecode
            //                 and ssc.faccode = f.faccode
            //                 and ssc.depcode = d.depcode
            //                 and ssc.acadprogcode = ap.acadprogcode
            //                 and ssc.batchcode = b.batchcode
            //                 and ssc.regno = s.regno
            //                 order by s.SEMSTARTDATE desc";



            $query = "
            select ssc.regno, ssc.grade, ssc.coursecode, ssc.semcode, c.COURSENAME, c.CREDITHRS, facname, depname, acadprogname, batchname,studname, STUDENTIIUIEMAIL, studfathername, STUDPHONE, CGPA, STUDEMAIL, tss.REMARKS,tss.UPTONOWPERCENTAGe,tss.UPTONOWCRDHRATT
            from TBL_STUDSEMCOURS ssc, semester sem, tbl_course c,TBL_FACULTY f, TBL_DEPARTMENT d, TBL_ACADPROG ap, tbl_batch b, TBL_STUDENT s, TBL_STUDSEM tss
            where ssc.regno = '" . $request->regno . "'
            and ssc.semcode = sem.semcode
            and ssc.depcode = c.depcode
            and ssc.coursecode = c.coursecode
            and ssc.faccode = f.faccode
            and ssc.depcode = d.depcode
            and ssc.acadprogcode = ap.acadprogcode
            and ssc.batchcode = b.batchcode
            and ssc.regno = s.regno
            and tss.regno=s.regno
            and tss.faccode = s.faccode
            and tss.depcode = s.depcode
            and tss.batchcode = s.batchcode
            and tss.semcode = 'SUM-2024'
            and tss.faccode = ssc.faccode
            and tss.depcode = ssc.depcode
            and tss.batchcode = ssc.batchcode
            and tss.semcode = ssc.semcode
            and tss.regno=ssc.regno
            and tss.ATTENDANCESTATUS not in('L','S','U')
            order by sem.SEMSTARTDATE desc
            ";

            return DB::select($query);
        } else {
            return "Empty Reg No";
        }

        return;
    }



    public function getStudentIntimation()
    {
        $request = json_decode(request()->getContent());
        if ($request && $request->regno) {

            // $query = "
            // select ssc.regno, ssc.grade, ssc.coursecode, ssc.semcode, c.COURSENAME, c.CREDITHRS, facname, depname, acadprogname, batchname,studname, STUDENTIIUIEMAIL, studfathername, STUDPHONE, CGPA, STUDEMAIL, tss.REMARKS,tss.UPTONOWPERCENTAGe,tss.UPTONOWCRDHRATT
            // from TBL_STUDSEMCOURS ssc, semester sem, tbl_course c,TBL_FACULTY f, TBL_DEPARTMENT d, TBL_ACADPROG ap, tbl_batch b, TBL_STUDENT s, TBL_STUDSEM tss
            // where ssc.regno = '".$request->regno."'
            // and ssc.semcode = sem.semcode
            // and ssc.depcode = c.depcode
            // and ssc.coursecode = c.coursecode
            // and ssc.faccode = f.faccode
            // and ssc.depcode = d.depcode
            // and ssc.acadprogcode = ap.acadprogcode
            // and ssc.batchcode = b.batchcode
            // and ssc.regno = s.regno
            // and tss.regno=s.regno
            // and tss.faccode = s.faccode
            // and tss.depcode = s.depcode
            // and tss.batchcode = s.batchcode
            // and tss.semcode = '".$request->semester."'
            // and tss.faccode = ssc.faccode
            // and tss.depcode = ssc.depcode
            // and tss.batchcode = ssc.batchcode
            // and tss.semcode = ssc.semcode
            // and tss.regno=ssc.regno
            // and tss.ATTENDANCESTATUS not in('L','S','U')
            // order by sem.SEMSTARTDATE desc
            // ";
            // Dev Mannan: Query commented with Hameed sab on Oct-6-2022

            $depCode = " ";
            if ($request->semester == 'FALL-2024') {
                $depCode = " and d.depcode in (
                    'FOCCS80',
                    'FOCSE82',
                    'FASDCS0',
                    'FASDSE51',
                    'FSLDSL15',
                    'FSLDSL43',
                    'FOEELM85',
                    'FOEELM86',
                    'FOETED87',
                    'FOETED88',
                    'FUDDUD14',
                    'FASDTECE3',
                    'FETBSEE76',
                    'FETDCE68',
                    'FETDEE75',
                    'FETDEM54',
                    'FETDMECE47',
                    'FETDMECE48',
                    'FETDTECE37',
                    'FSSPSY34',
                    'FSSPSY58',
                    'FSSSOC38',
                    'FSSSOC60',
                    'FSSIAA64',
                    'FSSIAA65',
                    'FSSHIS36',
                    'FSSHPS61',
                    'FSSMMC33',
                    'FSSMCS59',
                    'FOCSE82',
                    'FOCSE83',
                    'FASDSE51',
                    'FASDSE52',
                    'FASDES32',
                    'FLLDENG17',
                    'FLLDURDU37',
                    'FLLDURD55',
                    'FLLDPER57',
                    'FLLDENG56',
                    'FASDAM1',
                    'FARBDTI74',
                    'FASDAM1',
                    'FASDPHY45',
                    'FASDST49',
                    'FASDBIO78',
                    'FASDBIT53',
                    'FOCCS81',
                    'FMSDBA1',
                    'FMSDTM0',
                    'FMSDTM9',
                    'FMSTEM31',
                    'FMSDBA11',
                    'FECOIIIE16',
                    'DEM20',
                    'DEF21',
                    'FARBDARB13',
                    'SECODMRD39',
                    'FARBDTI74',
                    'SECODIBF22',
                    'DEF21',
                    'DEM20',
                    'FECOIBF40',
                    'FECOIIIE16',
                    'FECOMSEF41',
                    'SECODIBF22',
                    'SECODMRD39',
                    'FASDSE51',
                    'FASDSE52',
                    'FASCIRBS69',
                    'FASCIRBS70',
                    'FASDCS0',
                    'FASDCS6',
                    'FSSDPS12',
                    'FSSPIR63',
                    'FSSDPS12',
                    'FSSPIR63',
                    'FSSMCS59',
                    'FSSHPS61',
                    'FSSIAA64',
                    'FSSSOC38',
                    'FSSPSY58',
                    'FSSDDE77',
                    'FSSDED10',
                    'FSSSOC60') ";
                
            }

            $query = "
                select ssc.regno, ssc.grade, ssc.coursecode, ssc.semcode, c.COURSENAME, c.CREDITHRS, facname, depname, acadprogname, batchname,studname, STUDENTIIUIEMAIL, studfathername, STUDPHONE, CGPA, STUDEMAIL, tss.REMARKS,tss.UPTONOWPERCENTAGe,tss.UPTONOWCRDHRATT, tss.SEMCGPA
                from TBL_STUDSEMCOURS ssc, semester sem, tbl_batchsos c,TBL_FACULTY f, TBL_DEPARTMENT d, TBL_ACADPROG ap, tbl_batch b, TBL_STUDENT s, TBL_STUDSEM tss
                where ssc.regno = '" . $request->regno . "'
                $depCode
                and ssc.semcode = sem.semcode
                and ssc.depcode = c.depcode
                and ssc.coursecode = c.course_code
                AND (c.BATCHCODE = b.BATCHCODE)
                and ssc.faccode = f.faccode
                and ssc.depcode = d.depcode
                and ssc.acadprogcode = ap.acadprogcode
                and ssc.batchcode = b.batchcode
                and ssc.regno = s.regno
                and tss.regno=s.regno
                and tss.faccode = s.faccode
                and tss.depcode = s.depcode
                and tss.batchcode = s.batchcode
                and tss.semcode = '" . $request->semester . "'
                and tss.faccode = ssc.faccode
                and tss.depcode = ssc.depcode
                and tss.batchcode = ssc.batchcode
                and tss.semcode = ssc.semcode
                and tss.regno=ssc.regno
                and tss.ATTENDANCESTATUS not in('L','S','U')
                order by sem.SEMSTARTDATE desc
                ";
// return $query; exit;
            return DB::select($query);
        } else {
            return "Empty Reg No";
        }

        return;
    }


    public function permissionForm()
    {

        $request = json_decode(request()->getContent());
        if ($request && $request->regno) {
            // $query="select ssc.regno, ssc.grade, ssc.coursecode, ssc.semcode, c.COURSENAME, c.CREDITHRS from TBL_STUDSEMCOURS ssc, semester s, tbl_course c where ssc.regno = '".$request->regno."' and ssc.semcode = s.semcode and ssc.depcode = c.depcode and ssc.coursecode = c.coursecode order by s.SEMSTARTDATE desc";
            // $query = "select ssc.regno, ssc.grade, ssc.coursecode, ssc.semcode, c.COURSENAME, c.CREDITHRS, facname, depname, acadprogname, batchname,
            //             studname, STUDENTIIUIEMAIL, studfathername, STUDPHONE, CGPA, STUDEMAIL from TBL_STUDSEMCOURS ssc, semester s, tbl_course c,
            //             TBL_FACULTY f, TBL_DEPARTMENT d, TBL_ACADPROG ap, tbl_batch b, TBL_STUDENT s
            //              where ssc.regno = '" . $request->regno . "'
            //                 and ssc.semcode = s.semcode
            //                 and ssc.depcode = c.depcode
            //                 and ssc.coursecode = c.coursecode
            //                 and ssc.faccode = f.faccode
            //                 and ssc.depcode = d.depcode
            //                 and ssc.acadprogcode = ap.acadprogcode
            //                 and ssc.batchcode = b.batchcode
            //                 and ssc.regno = s.regno
            //                 order by s.SEMSTARTDATE desc";


            /* ******************************************************************************** */
            //                                   LAST QUERY BEFORE UPDATE
            /* ******************************************************************************** */

            // $query = "select CREDHRATTEMPT, UPTONOWCRDHRATT, GPA, REMARKS, SEMCRDHRATT, UPTONOWPERCENTAGE, OVERALLPERCENTAGE, ssc.regno, ssc.grade, ssc.coursecode, ssc.semcode, c.COURSENAME, c.CREDITHRS, facname, depname, acadprogname, batchname,
            // studname, STUDENTIIUIEMAIL, studfathername, STUDPHONE, CGPA, STUDEMAIL from TBL_STUDSEMCOURS ssc, semester s, tbl_course c,
            // TBL_FACULTY f, TBL_DEPARTMENT d, TBL_ACADPROG ap, tbl_batch b, TBL_STUDENT s, tbl_studsem ss
            //  where ssc.regno = '" . $request->regno . "'
            //     and ssc.semcode = s.semcode
            //     and ssc.depcode = c.depcode
            //     and ssc.coursecode = c.coursecode
            //     and ssc.faccode = f.faccode
            //     and ssc.depcode = d.depcode
            //     and ssc.acadprogcode = ap.acadprogcode
            //     and ssc.batchcode = b.batchcode
            //     and ssc.regno = s.regno
            //     and ssc.regno = ss.regno
            //     and ssc.semcode = ss.semcode
            //     order by s.SEMSTARTDATE desc";

            /* ******************************************************************************** */
            //                                QUERY UPDATED BY HAMEED
            /* ******************************************************************************** */
            $query = "select CREDHRATTEMPT, UPTONOWCRDHRATT, GPA, REMARKS, SEMCRDHRATT, UPTONOWPERCENTAGE, OVERALLPERCENTAGE, ssc.regno, ssc.grade, ssc.coursecode, ssc.semcode, bs.COURSENAME, c.CREDITHRS, facname, depname, acadprogname, batchname,
            studname, STUDENTIIUIEMAIL, studfathername, STUDPHONE, CGPA, STUDEMAIL from TBL_STUDSEMCOURS ssc, semester s, tbl_course c,
            TBL_FACULTY f, TBL_DEPARTMENT d, TBL_ACADPROG ap, tbl_batch b, TBL_STUDENT s, tbl_studsem ss, tbl_batchsos bs
             where ssc.regno = '" . $request->regno . "' 
                and ssc.semcode = s.semcode
                and ssc.depcode = c.depcode
                and ssc.coursecode = c.coursecode
                and ssc.faccode = f.faccode
                and ssc.depcode = d.depcode
                and ssc.acadprogcode = ap.acadprogcode
                and ssc.batchcode = b.batchcode
                and ssc.regno = s.regno
                and ssc.regno = ss.regno
                and ssc.semcode = ss.semcode
                and ssc.coursecode = bs.course_code
                and b.batchcode = bs.batchcode
                order by s.SEMSTARTDATE desc";
            // dd($query); exit();
            return DB::select($query);
        } else {
            return "Empty Reg No";
        }

        return;
    }


    public function paidAmount()
    {


        $request = json_decode(request()->getContent());
        if ($request && $request->regno) {
            // $query="select ssc.regno, ssc.grade, ssc.coursecode, ssc.semcode, c.COURSENAME, c.CREDITHRS from TBL_STUDSEMCOURS ssc, semester s, tbl_course c where ssc.regno = '".$request->regno."' and ssc.semcode = s.semcode and ssc.depcode = c.depcode and ssc.coursecode = c.coursecode order by s.SEMSTARTDATE desc";
            $query = "select pc.regno, pc.semcode,
                feedesc, pc.CHALLANNO,pc.CHALLANPAIDDATE, CHALLANPAIDAMNT
                from paychallan pc, paychallandetail pcd, feecodes fc
                where pc.regno = '" . $request->regno . "'
                and pc.semcode = 'FALL-2023'
                and pc.regno = pcd.regno
                and pc.semcode = pcd.semcode
                and pc.CHALLANNO = pcd.CHALLANNO
                and pcd.feecode = fc.feecode";
            return DB::select($query);
        } else {
            return "Empty Reg No";
        }

        return;
    }
    /* ********************************************************************************************* */
}
