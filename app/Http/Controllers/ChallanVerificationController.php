<?php

namespace App\Http\Controllers;

use App\AljamiaPayChallan;
use Illuminate\Http\Request;

class ChallanVerificationController extends Controller
{
    public function index()
    {
    	$request = json_decode(request()->getContent());
    	if ( $request ) {
    		$challanNumbers = $request->challans;
    		return AljamiaPayChallan::with(['detail', 'user', 'detail.feeCode'])->whereIn('challanno', $challanNumbers)->get();	
    	}
    	return;
    }

    public function verifyChallans()
    {
    	$request = json_decode(request()->getContent());
    	if ( $request ) {
    		$challanNumbers = $request->challans;
            $aljamiaUserLogin = $request->aljamiaUserLogin;
    		$currentDate = strtoupper(Date('d-M-Y'));
    		$currentTimeStamp = strtoupper(Date('d-M-Y h:i:s a'));
    		foreach ( $challanNumbers as $challanNumber ) {
    			$payChallan = AljamiaPayChallan::where('challanno', $challanNumber)
                    ->where('semcode', $request->semCode)
    				->first();
    			if ($payChallan) {
                    if (! $payChallan->challanpaiddatetime) {
                        \DB::table('paychallan')
                        ->where('challanno', $challanNumber)
                        ->where('semcode', $request->semCode)
                        ->update([
                            'CHALLANPAIDAMNT' => $payChallan->challanamnt,
                            'CHALLANPAIDDATE' => $currentDate,
                            'CHALLANPAIDDATETIME' => $currentTimeStamp,
                            'CHALLANVERIFIEDBY' => $aljamiaUserLogin
                        ]);
                        return 'Fee Verified successfully';    
                    }
                    return 'Fee already verified';
    			} //ending if
    		} //ending foreach
    	} //ending request
    	return 'Unable to parse data';
    }
}
