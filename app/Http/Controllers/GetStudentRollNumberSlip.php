<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class GetStudentRollNumberSlip extends Controller
{
    public function index()
    {
        $payload = json_decode(request()->getContent());
        $regNumber = $payload->regno;
        $currentSemester = $this->currentSemester();
        $response = DB::connection('oracle')->select("
    		SELECT
                s.*, p.*, pd.*, fc.*, d.depname, f.facname
            FROM
                paychallan p, paychallandetail pd, tbl_hstudents s, feecodes fc, tbl_faculty f, tbl_department d
            WHERE
                p.regno = pd.regno
            AND
                p.regno = '$regNumber'
            AND
                p.SEMCODE = '$currentSemester'
            AND
                p.semcode = pd.semcode
            AND
                p.CHALLANNO = pd.CHALLANNO
            AND
                s.regno = p.regno
            AND
                pd.feecode = fc.feecode
            AND
                s.depcode = d.depcode
            AND
                s.faccode = f.faccode
        ");
        return $response;
    }
    // Code for Rollnumber slip ends
    public function currentSemester()
    {
        // return 'SPR-2021';
        // return 'SUM-2021';
        // return 'FALL-2021';
        // return 'SPR-2022';
        // return 'FALL-2022';
        return 'FALL-2024';
    }

    public function testisdefualter(){
        //$payload = json_decode ( request()->getContent() );
        $regNumber = request('regno'); //$payload->regno;
        $currentSemester = $this->currentSemester();
        $SQL="SELECT * FROM TBL_STUDSEM WHERE regno='$regNumber' AND semcode='$currentSemester'" ;
        $studinfo = DB::connection('oracle')->select($SQL);
        echo $SQL;
        dd($studinfo);
        if(!empty($studinfo) && strtolower(trim($studinfo[0]->attendancestatus))!='l'){
            return 0;
        }else{
            return 1;
        }
    }

    /* **************************************************************************************** */
    /*                                  IS DEFAULTER                                            */
    /* **************************************************************************************** */
    public function isdefualter(){
        $payload = json_decode ( request()->getContent() );
        $regNumber = $payload->regno;
        $currentSemester = $this->currentSemester();
        $SQL="SELECT * FROM TBL_STUDSEM WHERE regno='$regNumber' AND semcode='$currentSemester'" ;
        $studinfo = DB::connection('oracle')->select($SQL);
        if(!empty($studinfo) && strtolower(trim($studinfo[0]->attendancestatus))!='l'){
            return 0;
        }else{
            return 1;
        }
    }
    /* **************************************************************************************** */
    /*                                  IS PREVENTED                                            */
    /* **************************************************************************************** */
    public function isPrevented(){
        $payload = json_decode ( request()->getContent() );
        $regNumber = $payload->regno;
        $currentSemester = $this->currentSemester();
        // $SQL="SELECT * FROM TBL_STUDSEM WHERE regno='$regNumber' AND semcode='$currentSemester'" ;
        $SQL="SELECT * FROM tbl_studsemcourse WHERE regno='$regNumber' AND semcode='$currentSemester'" ;
        $studinfo = DB::connection('oracle')->select($SQL);
        if(!empty($studinfo) && strtolower(trim($studinfo[0]->CRSPREVENTSTATUS))!='p'){
            return 0;
        }else{
            // dd($studinfo[0]->CRSPREVENTSTATUS);
            return 1;
        }
    }

    public function TestisAdmissionCancelled(){
        //$payload = json_decode ( request()->getContent() );
        $regNumber =  request('regno'); //$payload->regno;
        $currentSemester = $this->currentSemester();
       echo $SQL="SELECT * FROM TBL_STUDSEM WHERE regno='$regNumber' AND semcode='$currentSemester'" ;
        $studinfo = DB::connection('oracle')->select($SQL);
        if(!empty($studinfo[0]->attendancestatus) && strtolower(trim($studinfo[0]->attendancestatus)) != 'cancelled'){
            return 0;
        }else{
            dd($studinfo[0]->attendancestatus);
            return 1;
        }
    }
    /* **************************************************************************************** */
    /*                                  ADMISSION CANCELLED                                     */
    /* **************************************************************************************** */
    public function isAdmissionCancelled(){
        $payload = json_decode ( request()->getContent() );
        $regNumber = $payload->regno;
        $currentSemester = $this->currentSemester();
        $SQL="SELECT * FROM TBL_STUDSEM WHERE regno='$regNumber' AND semcode='$currentSemester'" ;
        $studinfo = DB::connection('oracle')->select($SQL);
        if(!empty($studinfo) && strtolower(trim($studinfo[0]->attendancestatus)) != 'cancelled'){
            return 0;
        }else{
            return 1;
        }
    }




    // Code for Rollnumber slip starts
    public function getslipByRegno()
    {
    	$payload = json_decode ( request()->getContent() );
    	$regNumber = $payload->regno;
		$currentSemester = $this->currentSemester(); //'FALL-2021';

		$studinfo = DB::connection('oracle')->select("
    		SELECT
                s.regno, s.studname, s.studfathername, s.depcode, s.faccode, s.acadprogcode, s.batchcode
            FROM
                tbl_student s
            WHERE
                s.regno = '$regNumber'
          /*  AND
                s.batchcode = s.batchcode */
        ");

		if(!empty($studinfo)){
		$faccode = $studinfo[0]->faccode;
		$depcode = $studinfo[0]->depcode;
		$acadprogcode = $studinfo[0]->acadprogcode;
		$batchcode = $studinfo[0]->batchcode;

    	$response = DB::connection('oracle')->select("
    		SELECT
                s.*, p.*
            FROM
                tbl_studsemcourse p, tbl_student s
            WHERE
                p.regno = s.regno
		    AND
			    p.batchcode = s.batchcode
			AND
			    p.depcode = s.depcode
			AND
			    p.faccode = s.faccode
			AND
			    p.acadprogcode = s.acadprogcode
			AND
			    p.regno = '$regNumber'
            AND
                p.semcode = '$currentSemester'
			 AND
                p.batchcode = '$batchcode'
			 AND
                p.faccode = '$faccode'
			 AND
                p.acadprogcode = '$acadprogcode'
			 AND
                p.depcode = '$depcode'

           ");
		}
        return $response;
    }

    public function getpicByRegno()
    {
    	$payload = json_decode ( request()->getContent() );
    	$regNumber = $payload->regno;
    	$response = DB::connection('oracle')->select("
    		SELECT
                s.regno, s.studpic
            FROM
                studentpicturelr s
            WHERE
                s.regno = '$regNumber'
        ");
		if(!empty($response)){
        return strval($response[0]->studpic);
		}
		else{
			  $response = '';
		}
    }

    public function getfaculty()
    {
    	$payload = json_decode ( request()->getContent() );
    	$faccode = $payload->faccode;

    	$response = DB::connection('oracle')->select("
    		SELECT
                f.facname
            FROM
                tbl_faculty f
            WHERE
                f.faccode = '$faccode'
        ");
        return $response;
    }

    public function getacadprograme()
    {
    	$payload = json_decode ( request()->getContent() );
    	$acadprogcode = $payload->acadprogcode;

    	$response = DB::connection('oracle')->select("
    		SELECT
                a.acadprogname
            FROM
                tbl_acadprog a
            WHERE
                a.acadprogcode = '$acadprogcode'
        ");
        return $response;
    }

    public function getbatch()
    {
    	$payload = json_decode ( request()->getContent() );
    	$batchcode = $payload->batchcode;

    	$response = DB::connection('oracle')->select("
    		SELECT
                b.batchname
            FROM
                tbl_batch b
            WHERE
                b.batchcode = '$batchcode'
        ");
        return $response;
    }

    public function getsemDate()
    {
    	$payload = json_decode ( request()->getContent() );
    	$semcode = $payload->semcode;

    	$response = DB::connection('oracle')->select("
    		SELECT
                sem.*
            FROM
                semester sem
            WHERE
                sem.semcode = '$semcode'
        ");
        return $response;
    }
}
