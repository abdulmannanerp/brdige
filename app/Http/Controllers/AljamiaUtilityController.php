<?php

namespace App\Http\Controllers;

use App\AljamiaBatch;
use App\AljamiaDepartment;
use App\AljamiaFaculty;
use App\AljamiaFeeCodes;
use App\AljamiaProgram;
use App\AljamiaSemester;
use Illuminate\Http\Request;

class AljamiaUtilityController extends Controller
{
    public function feeCodes()
    {
        return AljamiaFeeCodes::where('feetypecode', 101)->get();
        // return AljamiaFeeCodes::whereIn('feetypecode', [7,10])->get();
    }

    public function getTables()
    {
    	return \DB::select('select * from tab');
    }

    public function faculty()
    {
    	return AljamiaFaculty::all();
    }

    public function department()
    {
    	return AljamiaDepartment::all();
    }

    public function program()
    {
    	return AljamiaProgram::all();
    }

    public function batch()
    {
    	return AljamiaBatch::all();
    }

    public function semester()
    {
    	return AljamiaSemester::all();
    }
}
