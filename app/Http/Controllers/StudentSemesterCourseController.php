<?php

namespace App\Http\Controllers;

use App\AljamiaBatch;
use App\AljamiaStudentSemesterCourse;
use Illuminate\Http\Request;

class StudentSemesterCourseController extends Controller
{
    public function index()
    {
        $batch = AljamiaBatch::where('batchcode', request('batchCode'))->first();
        return AljamiaStudentSemesterCourse::with(['student', 'batch'])
            ->whereHas('student', function($query) use ($batch) {
                $query->where('sex', $batch->sex);
            })
            ->where('coursecode', request('courseCode'))
            ->where('semcode', request('semester'))
            ->get();
            // ->where('acadprogcode', request('program'))
            // ->where('depcode', request('departmentCode'))
            // ->where('faccode', request('facultyCode'))
            // ->whereIn('batchcode', request('batchCodes'))
    }
}
