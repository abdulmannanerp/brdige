<?php

namespace App\Http\Controllers;

use App\AljamiaStudent;
use DB;
class AljamiaStdPermissionController extends Controller
{
    /* ********************************************************************************************* */
    //                                     GET STUDENT PICTURE
    /* ********************************************************************************************* */
    public function getStudentPicture(){
        // return array("A","B");
         
        $request = json_decode(request()->getContent());
        if ($request && $request->regno) {
            $query = "select regno, STUDPIC from STUDENTPICTURELR
                         where regno = '".$request->regno."'";
            return DB::connection('oracle') ->select($query);    
        }else{
            return "Empty Picture Against Reg No";
        }

        return;
    }
    /* ********************************************************************************************* */
    //                                    GET STUDENT GPA/REMARKS/CrHrs
    /* ********************************************************************************************* */
    // public function getStudentGpaRemarksCrHrs(){         
    //     $request = json_decode(request()->getContent());
    //     if ($request && $request->regno) {
    //         $query = "select distinct SEMESTER.SEMSTARTDATE, TBL_STUDSEM.semcode,  
    //                     TBL_STUDSEM.UPTONOWPERCENTAGE, TBL_STUDSEM.UPTONOWCRDHRATT,
    //                     SUBSTR(TBL_STUDSEM.GPA, 1, INSTR(TBL_STUDSEM.GPA, '.')+2) RND_STUDSEM_GPA, 
    //                     TBL_STUDSEM.REMARKS, SUBSTR(TBL_STUDSEM.SEMCGPA, 1, INSTR(TBL_STUDSEM.SEMCGPA, '.')+2) RND_STUDSEM_SEMCGPA
    //                     from tbl_studsemcourse ssc, TBL_STUDSEM, SEMESTER, TBL_STUDENT s        
    //                     where ssc.regno = '".$request->regno."'
    //                     and TBL_STUDSEM.regno = ssc.regno
    //                     and TBL_STUDSEM.semcode = SEMESTER.semcode
    //                     order by SEMESTER.SEMSTARTDATE asc";
    //         return DB::connection('oracle') ->select($query);    
    //     }else{
    //         return "Empty Record GPA and Remarks";
    //     }

    //     return;
    // }
    /* ********************************************************************************************* */
    //                                 GET STUDENT CGPA/Overall Percentage
    /* ********************************************************************************************* */
    // public function getStudentCgpaOverallPercentage()
    // {
         
    //     $request = json_decode(request()->getContent());
    //     if ($request && $request->regno) {
    //         $query = "select distinct SUBSTR(TBL_STUDENT.CGPA, 1, INSTR(TBL_STUDENT.CGPA, '.')+2) RND_STUDENT_CGPA,
    //                     round(TBL_STUDENT.OVERALLPERCENTAGE, 2)  RND_STUDENT_OVERALLPERCENTAGE
    //                     from TBL_STUDENT       
    //                     where regno = '".$request->regno."'";
    //         return DB::select($query);    
    //     }else{
    //         return "Empty Record Overall Percentage and CGPA";
    //     }

    //     return;
    // }
    /* ********************************************************************************************* */
    //                                     GET STUDENT COURSES
    /* ********************************************************************************************* */
    public function getStudentCourses(){
         
        $request = json_decode(request()->getContent());
        if ($request && $request->regno) {
            // $query="select ssc.regno, ssc.grade, ssc.coursecode, ssc.semcode, c.COURSENAME, c.CREDITHRS from TBL_STUDSEMCOURS ssc, semester s, tbl_course c where ssc.regno = '".$request->regno."' and ssc.semcode = s.semcode and ssc.depcode = c.depcode and ssc.coursecode = c.coursecode order by s.SEMSTARTDATE desc";
            $query = "
			select ssc.regno, ssc.grade, ssc.coursecode, ssc.semcode, c.COURSENAME, c.CREDITHRS, facname, depname, acadprogname, batchname,studname, STUDENTIIUIEMAIL, studfathername, STUDPHONE, CGPA, STUDEMAIL, tss.REMARKS,tss.UPTONOWPERCENTAGe,tss.UPTONOWCRDHRATT
from TBL_STUDSEMCOURS ssc, semester sem, tbl_course c,TBL_FACULTY f, TBL_DEPARTMENT d, TBL_ACADPROG ap, tbl_batch b, TBL_STUDENT s, TBL_STUDSEM tss
where ssc.regno = '".$request->regno."'
and ssc.semcode = sem.semcode
and ssc.depcode = c.depcode
and ssc.coursecode = c.coursecode
and ssc.faccode = f.faccode
and ssc.depcode = d.depcode
and ssc.acadprogcode = ap.acadprogcode
and ssc.batchcode = b.batchcode
and ssc.regno = s.regno
and tss.regno=s.regno
and tss.faccode = s.faccode
and tss.depcode = s.depcode
and tss.batchcode = s.batchcode
and tss.semcode = 'SUM-2022'
and tss.faccode = ssc.faccode
and tss.depcode = ssc.depcode
and tss.batchcode = ssc.batchcode
and tss.semcode = ssc.semcode
and tss.regno=ssc.regno
and tss.ATTENDANCESTATUS not in('L','S','U')
order by sem.SEMSTARTDATE desc
			";
            return DB::select($query);    
        }else{
            return "Empty Reg No";
        }

        return;
    }
    /* ********************************************************************************************* 
and tss.ATTENDANCESTATUS not in('L','S','U')
and tss.faccode not in('FMS1','FSS5')
and tss.faccode not in('FMS1','FARB6', 'FSL8', 'FUD7')
    */
}
