<?php

namespace App\Http\Controllers;

use App\AljamiaPayChallan;
use App\AljamiaStudent;
use Illuminate\Http\Request;

class SearchController extends Controller
{
    public function index()
    {
    	$searchBy = request('searchBy');
    	$query = request('query');
    	switch ($searchBy) {
            case 'regno':
                return $this->getResultsByRegistrationNumber($query);
                break;
            case 'challan':
                return $this->getResultsByChallanNumber($query);
                break;
            case 'email':
                return $this->getResultsByEmailAddress($query);
                break;
        }
    }

    public function getResultsByRegistrationNumber($query)
    {
    	return AljamiaStudent::where('regno',$query)->first();
    }

    public function getResultsByChallanNumber($query)
    {
    	return AljamiaPayChallan::with(['detail'])->where('challanno', $query)->first();
    }

    public function getResultsByEmailAddress($query)
    {
    	return AljamiaStudent::where('studentiiuiemail', $query)
    		->orWhere('studemail', $query)->first();
    }
}
