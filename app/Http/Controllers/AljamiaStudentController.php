<?php

namespace App\Http\Controllers;

use App\AljamiaStudent;
use DB;
class AljamiaStudentController extends Controller
{
    public function findStudentByEmail()
    {
        $request = json_decode(request()->getContent());
        if ($request && $request->email) {
            return AljamiaStudent::where('studentiiuiemail', $request->email)
                ->orWhere('studemail',$request->email)->first();
        }
        return;
    }

    public function findStudentByRegistrationNumber()
    {
        $request = json_decode(request()->getContent());
        if ($request && $request->regno) {
            return AljamiaStudent::where('regno', $request->regno)->first();
        }
        return;
    }
    public function getStudentsByBatch()
    {
        $request = json_decode(request()->getContent());
        return AljamiaStudent::where('batchcode', $request->batch)->get();
    }

    public function getStudentByNameAndCnic()
    {
        $request = json_decode(request()->getContent());
        $response=AljamiaStudent::query()
            ->where('studname', 'like', '%'.$request->name.'%')
            ->where(function($query) use ($request) {
                $query->where('nidpassno', $request->cnic)
                    ->orWhere('nidpassno', $request->cnicWithoutDashes);
            })
            ->get();
		if($response->count() <=0 && strlen($request->cnicWithoutDashes) == 13){
			$cnic=$request->cnicWithoutDashes;
			 $part1 = substr($cnic, 0, 5);
			$part2 = substr($cnic, 5, 7);
			$part3 = substr($cnic, -1);

			$cnic = $part1 . '-' . $part2 . '-' . $part3;
			$response=AljamiaStudent::query()
            ->where('studname', 'like', '%'.$request->name.'%')
            ->where(function($query) use ($cnic) {
                $query->where('nidpassno', $cnic)
                    ->orWhere('nidpassno', $cnic);
            })
            ->get();
		}
		if($response->count()>1){
			$response=$response[$response->count()-1];
		}else if($response->count()==1){
			$response=$response[0];
		}
		return $response;
    }


    public function getStudentByNameAndCnictest()
    {
        $response=AljamiaStudent::query()
            ->where('studname', 'like', '%Faria Fatima%')
            ->where(function($query){
                $query->where('nidpassno', '3310055285500')
                    ->orWhere('nidpassno', '3310055285500');
            })
            ->get();
		if($response->count() <=0 && strlen('3310055285500') == 13){
			$cnic='3310055285500';
			 $part1 = substr($cnic, 0, 5);
			$part2 = substr($cnic, 5, 7);
			$part3 = substr($cnic, -1);

			$cnic = $part1 . '-' . $part2 . '-' . $part3;
			$response=AljamiaStudent::query()
            ->where('studname', 'like', '%Faria Fatima%')
            ->where(function($query) use ($cnic){
                $query->where('nidpassno', $cnic)
                    ->orWhere('nidpassno', $cnic);
            })
            ->get();
		}
		if($response->count()>1){
			$response=$response[$response->count()-1];
		}else if($response->count()==1){
			$response=$response[0];
		}
		return $response;
    }
}
