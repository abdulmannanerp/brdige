<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\AljamiaStudent;

class GetChallanInformationFromDb extends Controller
{
    public function index()
    {
        $payload = json_decode(request()->getContent());
        $regNumber = $payload->regno;
        $currentSemester = $this->currentSemester();
        $response = DB::connection('oracle')->select("
    		SELECT
                s.*, p.*, pd.*, fc.*, d.depname, f.facname, p.bankname2 as bk2name, p.bankaccno2 as bkacc
            FROM
                paychallan p, paychallandetail pd, tbl_hstudents s, feecodes fc, tbl_faculty f, tbl_department d
            WHERE
                p.regno = pd.regno
            AND
                p.regno = '$regNumber'
            AND
                p.SEMCODE = '$currentSemester'
            AND
                p.semcode = pd.semcode
            AND
                p.CHALLANNO = pd.CHALLANNO
            AND
                s.regno = p.regno
            AND
                pd.feecode = fc.feecode
            AND
                s.depcode = d.depcode
            AND
                s.faccode = f.faccode
        ");
        return $response;
    }
    public function qrCode(){

        $payload = json_decode(request()->getContent());
        $qr = $payload->qr;
        $regno = $payload->regno;


        // $sql = "INSERT INTO qrtest (regno, qr_transcript_img) VALUES ('".$regno."', '".base64_decode($qr)."')";
        // $sql="UPDATE tbl_student SET qr_transcript='".$qr."', qr_img='".base64_decode($qr)."' where regno ='".$regno."'";
        // $sql="UPDATE tbl_student SET qr_transcript='".$qr."' where regno ='".$regno."'";

        $decodedData = base64_decode($qr);

        $transformed = DB::table('tbl_student')
        ->where('regno', $regno)
        ->update(['qr_transcript' => $qr, 'qr_img' => $decodedData, 'qr_transcript_img' => $decodedData]);
        return $transformed;
        // $transformed=DB::update($sql);
        // if($transformed){
        //     return 1;
        // }else{
        //     return 2;
        // }
    }
    // Code for Rollnumber slip ends
    public function currentSemester()
    {
        // return 'SPR-2021';
        // return 'SUM-2021';
        // return 'FALL-2021';
        // return 'FALL-2022';
        return 'SPR-2025';
    }

    /**
     * Collect information of challans generated in last hour.
     * This detail will then be sent to ABL
     */
    public function getChallansGeneratedInPastHour()
    {
        $currentSemester = $this->currentSemester();

        $dateAndTimeNow = Date('d-m-Y H:i:s');
        $dateNow = strtoupper(Date('d-M-Y').'%');
        $hourNow = Date('H');

        /**
        * We need to -1 the hour because cron that will run at 10:00 needs to send the challan that was
        * generated between 9 to 10
        */
        $previousChallanHour = sprintf("%02d", $hourNow - 1);
        return $records =  \DB::connection('abl')
            ->select(
                "select * from (
				select
					REGNO,
					STUDNAME,
					SEX,SEMCODE,
					CHALLANNO,
					CHALLANDATE,
					CHALLANDUEDATE,
					CHALLANAMNT,
					CHALLANPAIDAMNT,
					CHALLANPAIDDATE,
					PAYMENTREFNO,
					CHALLANSTATUS,
					BANKACCNO,
					BANKNAME,
                    BANKNAME2,
					FACCODE,
					DEPCODE,
					ACADPROGCODE,
					BATCHCODE,
					BATCHNAME,
					ACADPROGNAME,
					DEPNAME,
					FACNAME,
					ENTRYTIME,
					challandatetime,
					to_char(to_date(substr(challandatetime, 0, 11), 'DD-MM-YYYY'), 'DD-MON-YYYY') as gendate,
					to_char(to_date(substr(challandatetime, 12, length(challandatetime)), 'HH24:MI:SS'), 'HH24:MI:SS') as gentime,
					to_char(to_date(substr(challandatetime, 12, length(challandatetime)), 'HH24:MI:SS'), 'HH24') as genhour,
					to_char(to_date(substr(challandatetime, 12, length(challandatetime)), 'HH24:MI:SS'), 'MI') as genmin ,
					to_char(to_date(substr(challandatetime, 12, length(challandatetime)), 'HH24:MI:SS'), 'SS') as gensec
				FROM feechallanpayment
				)
			WHERE semcode = '$currentSemester' and (upper(BANKNAME) like '%ALLIED%' or upper(BANKNAME2) like '%ALLIED%') and gendate LIKE '$dateNow' and genhour = '$previousChallanHour'"
            );
    }

/* ****************************************************************************** */
//                              FAYSAL BANK BRIDGE START
/* ****************************************************************************** */
     public function getChallansGeneratedInPastHourFaysal()
    {
        $currentSemester = $this->currentSemester();
        $dateAndTimeNow = Date('d-m-Y H:i:s');
        $dateNow = strtoupper(Date('d-M-Y').'%');
        $hourNow = Date('H');

        /**
        * We need to -1 the hour because cron that will run at 10:00 needs to send the challan that was
        * generated between 9 to 10
        */
        $previousChallanHour = sprintf("%02d", $hourNow - 1);
        //Change 'abl' to 'faysal'
        return $records =  \DB::connection('abl')
            ->select(
                "select * from (
                select
                    REGNO,
                    STUDNAME,
                    SEX,SEMCODE,
                    CHALLANNO,
                    CHALLANDATE,
                    CHALLANDUEDATE,
                    CHALLANAMNT,
                    CHALLANPAIDAMNT,
                    CHALLANPAIDDATE,
                    PAYMENTREFNO,
                    CHALLANSTATUS,
                    bankaccno2,
                    bankname2,
                    FACCODE,
                    DEPCODE,
                    ACADPROGCODE,
                    BATCHCODE,
                    BATCHNAME,
                    ACADPROGNAME,
                    DEPNAME,
                    FACNAME,
                    ENTRYTIME,
                    challandatetime,
                    to_char(to_date(substr(challandatetime, 0, 11), 'DD-MM-YYYY'), 'DD-MON-YYYY') as gendate,
                    to_char(to_date(substr(challandatetime, 12, length(challandatetime)), 'HH24:MI:SS'), 'HH24:MI:SS') as gentime,
                    to_char(to_date(substr(challandatetime, 12, length(challandatetime)), 'HH24:MI:SS'), 'HH24') as genhour,
                    to_char(to_date(substr(challandatetime, 12, length(challandatetime)), 'HH24:MI:SS'), 'MI') as genmin ,
                    to_char(to_date(substr(challandatetime, 12, length(challandatetime)), 'HH24:MI:SS'), 'SS') as gensec
                FROM feechallanpayment
                )
            WHERE semcode = '$currentSemester' and bankname2 like '%Faysal Bank Limited%' and sex='M' and gendate LIKE '$dateNow' and genhour = '$previousChallanHour'"
            );

            // WHERE semcode = 'SPR-2024' and upper(BANKNAME) like '%FAYSAL%' and sex='M' and gendate LIKE '$dateNow' and genhour = '$previousChallanHour'"            
    }
/* ****************************************************************************** */
//                                      END
/* ****************************************************************************** */

public function getChallansGeneratedInPastHourAlfalah()
{
    $currentSemester = $this->currentSemester();
    $dateAndTimeNow = Date('d-m-Y H:i:s');
    $dateNow = strtoupper(Date('d-M-Y').'%');
    $hourNow = Date('H');

    /**
    * We need to -1 the hour because cron that will run at 10:00 needs to send the challan that was
    * generated between 9 to 10
    */
    $previousChallanHour = sprintf("%02d", $hourNow - 1);
    //Change 'abl' to 'faysal'
    return $records =  \DB::connection('abl')
        ->select(
            "select * from (
            select
                REGNO,
                STUDNAME,
                SEX,SEMCODE,
                CHALLANNO,
                CHALLANDATE,
                CHALLANDUEDATE,
                CHALLANAMNT,
                CHALLANPAIDAMNT,
                CHALLANPAIDDATE,
                PAYMENTREFNO,
                CHALLANSTATUS,
                bankaccno2,
                bankname2,
                FACCODE,
                DEPCODE,
                ACADPROGCODE,
                BATCHCODE,
                BATCHNAME,
                ACADPROGNAME,
                DEPNAME,
                FACNAME,
                ENTRYTIME,
                challandatetime,
                to_char(to_date(substr(challandatetime, 0, 11), 'DD-MM-YYYY'), 'DD-MON-YYYY') as gendate,
                to_char(to_date(substr(challandatetime, 12, length(challandatetime)), 'HH24:MI:SS'), 'HH24:MI:SS') as gentime,
                to_char(to_date(substr(challandatetime, 12, length(challandatetime)), 'HH24:MI:SS'), 'HH24') as genhour,
                to_char(to_date(substr(challandatetime, 12, length(challandatetime)), 'HH24:MI:SS'), 'MI') as genmin ,
                to_char(to_date(substr(challandatetime, 12, length(challandatetime)), 'HH24:MI:SS'), 'SS') as gensec
            FROM feechallanpayment
            )
        WHERE semcode = '$currentSemester' and upper(bankname2) like '%FALAH%' and sex='F' and gendate LIKE '$dateNow' and genhour = '$previousChallanHour'"
        );

        // WHERE semcode = 'SPR-2024' and upper(BANKNAME) like '%FAYSAL%' and sex='M' and gendate LIKE '$dateNow' and genhour = '$previousChallanHour'"            
}

    public function getChallansGeneratedInPastHourFwbl()
    {
        $currentSemester = $this->currentSemester();
        $dateAndTimeNow = Date('d-m-Y H:i:s');
        $dateNow = strtoupper(Date('d-M-Y').'%');
        $hourNow = Date('H');

        /**
        * We need to -1 the hour because cron that will run at 10:00 needs to send the challan that was
        * generated between 9 to 10
        */
        $previousChallanHour = sprintf("%02d", $hourNow - 1);
        return $records =  \DB::connection('abl')
            ->select(
                "select * from (
				select
					REGNO,
					STUDNAME,
					SEX,SEMCODE,
					CHALLANNO,
					CHALLANDATE,
					CHALLANDUEDATE,
					CHALLANAMNT,
					CHALLANPAIDAMNT,
					CHALLANPAIDDATE,
					PAYMENTREFNO,
					CHALLANSTATUS,
					BANKACCNO,
					BANKNAME,
					FACCODE,
					DEPCODE,
					ACADPROGCODE,
					BATCHCODE,
					BATCHNAME,
					ACADPROGNAME,
					DEPNAME,
					FACNAME,
					ENTRYTIME,
					challandatetime,
					to_char(to_date(substr(challandatetime, 0, 11), 'DD-MM-YYYY'), 'DD-MON-YYYY') as gendate,
					to_char(to_date(substr(challandatetime, 12, length(challandatetime)), 'HH24:MI:SS'), 'HH24:MI:SS') as gentime,
					to_char(to_date(substr(challandatetime, 12, length(challandatetime)), 'HH24:MI:SS'), 'HH24') as genhour,
					to_char(to_date(substr(challandatetime, 12, length(challandatetime)), 'HH24:MI:SS'), 'MI') as genmin ,
					to_char(to_date(substr(challandatetime, 12, length(challandatetime)), 'HH24:MI:SS'), 'SS') as gensec
				FROM feechallanpayment
				)
			WHERE semcode = '$currentSemester' and upper(BANKNAME) like '%WOMEN%' and sex='F' and gendate LIKE '$dateNow' and genhour = '$previousChallanHour'"
            );
    }

    public function getChallanByDate()
    {
        $content = json_decode(request()->getContent());
        if (! $content) {
            return 'Unable to get date';
        }

        $date = strtoupper($content->date).'%';
        $bankName = $content->bankName;
        $accNo = '';

        //echo "Bank: ".$bankName; exit;
        /* ****************************************************************************** */
        //                              FAYSAL BANK CONDITION START
        /* ****************************************************************************** */
        $semester = $this->currentSemester();
        if($bankName == 'fbl'){
            // $semester = $this->currentSemester();
        return $records =  \DB::connection('abl')
            ->select(
                "select * from (
                select
                    REGNO,
                    STUDNAME,
                    SEX,SEMCODE,
                    CHALLANNO,
                    CHALLANDATE,
                    CHALLANDUEDATE,
                    CHALLANAMNT,
                    CHALLANPAIDAMNT,
                    CHALLANPAIDDATE,
                    PAYMENTREFNO,
                    CHALLANSTATUS,
                    BANKACCNO,
                    BANKACCNO2,
                    BANKNAME,
                    bankname2,
                    FACCODE,
                    DEPCODE,
                    ACADPROGCODE,
                    BATCHCODE,
                    BATCHNAME,
                    ACADPROGNAME,
                    DEPNAME,
                    FACNAME,
                    ENTRYTIME,
                    challandatetime,
                    to_char(to_date(substr(challandatetime, 0, 11), 'DD-MM-YYYY'), 'DD-MON-YYYY') as gendate,
                    to_char(to_date(substr(challandatetime, 12, length(challandatetime)), 'HH24:MI:SS'), 'HH24:MI:SS') as gentime,
                    to_char(to_date(substr(challandatetime, 12, length(challandatetime)), 'HH24:MI:SS'), 'HH24') as genhour,
                    to_char(to_date(substr(challandatetime, 12, length(challandatetime)), 'HH24:MI:SS'), 'MI') as genmin ,
                    to_char(to_date(substr(challandatetime, 12, length(challandatetime)), 'HH24:MI:SS'), 'SS') as gensec
                FROM feechallanpayment
                )
            WHERE semcode = '$semester' $accNo and bankname2 like '%Faysal Bank Limited%' and sex='M' and gendate LIKE '$date'"
            );
        }
        //WHERE semcode = '$semester' and bankname2 like '%Faysal Bank Limited%' $accNo and sex='M' and gendate LIKE '$date'"
        /* ****************************************************************************** */
        //                                          END
        /* ****************************************************************************** */
        if($bankName == 'fwbl'){
            $accNo = " and upper(BANKNAME) like '%WOMEN%'";
        }
        if($bankName == 'abl'){
            $accNo = " and (upper(BANKNAME) like '%ALLIED%' or upper(BANKNAME2) like '%ALLIED%')";
        }
        if($bankName == 'alfalah'){

        return $records =  \DB::connection('abl')
            ->select(
                "select * from (
                select
                    REGNO,
                    STUDNAME,
                    SEX,SEMCODE,
                    CHALLANNO,
                    CHALLANDATE,
                    CHALLANDUEDATE,
                    CHALLANAMNT,
                    CHALLANPAIDAMNT,
                    CHALLANPAIDDATE,
                    PAYMENTREFNO,
                    CHALLANSTATUS,
                    bankaccno2,
                    bankname2,
                    FACCODE,
                    DEPCODE,
                    ACADPROGCODE,
                    BATCHCODE,
                    BATCHNAME,
                    ACADPROGNAME,
                    DEPNAME,
                    FACNAME,
                    ENTRYTIME,
                    challandatetime,
                    to_char(to_date(substr(challandatetime, 0, 11), 'DD-MM-YYYY'), 'DD-MON-YYYY') as gendate,
                    to_char(to_date(substr(challandatetime, 12, length(challandatetime)), 'HH24:MI:SS'), 'HH24:MI:SS') as gentime,
                    to_char(to_date(substr(challandatetime, 12, length(challandatetime)), 'HH24:MI:SS'), 'HH24') as genhour,
                    to_char(to_date(substr(challandatetime, 12, length(challandatetime)), 'HH24:MI:SS'), 'MI') as genmin ,
                    to_char(to_date(substr(challandatetime, 12, length(challandatetime)), 'HH24:MI:SS'), 'SS') as gensec
                FROM feechallanpayment
                )
            WHERE semcode = '$semester' and upper(bankname2) like '%FALAH%' and sex='F' and gendate LIKE '$date'"
            );

        }
        if($bankName == 'hbl'){
            return $records =  \DB::connection('abl')
            ->select(
                "select * from (
                select
                    REGNO,
                    STUDNAME,
                    SEX,SEMCODE,
                    CHALLANNO,
                    CHALLANDATE,
                    CHALLANDUEDATE,
                    CHALLANAMNT,
                    CHALLANPAIDAMNT,
                    CHALLANPAIDDATE,
                    PAYMENTREFNO,
                    CHALLANSTATUS,
                    BANKACCNO,
                    BANKNAME,
                    FACCODE,
                    DEPCODE,
                    ACADPROGCODE,
                    BATCHCODE,
                    BATCHNAME,
                    ACADPROGNAME,
                    DEPNAME,
                    FACNAME,
                    ENTRYTIME,
                    challandatetime,
                    to_char(to_date(substr(challandatetime, 0, 11), 'DD-MM-YYYY'), 'DD-MON-YYYY') as gendate,
                    to_char(to_date(substr(challandatetime, 12, length(challandatetime)), 'HH24:MI:SS'), 'HH24:MI:SS') as gentime,
                    to_char(to_date(substr(challandatetime, 12, length(challandatetime)), 'HH24:MI:SS'), 'HH24') as genhour,
                    to_char(to_date(substr(challandatetime, 12, length(challandatetime)), 'HH24:MI:SS'), 'MI') as genmin ,
                    to_char(to_date(substr(challandatetime, 12, length(challandatetime)), 'HH24:MI:SS'), 'SS') as gensec
                FROM feechallanpayment
                )
            WHERE upper(BANKNAME) like '%HABIB%' and sex='M' and gendate LIKE '$date'"
            );
        }
        $semester = $this->currentSemester();

        return $records =  \DB::connection('abl')
            ->select(
                "select * from (
                select
                    REGNO,
                    STUDNAME,
                    SEX,SEMCODE,
                    CHALLANNO,
                    CHALLANDATE,
                    CHALLANDUEDATE,
                    CHALLANAMNT,
                    CHALLANPAIDAMNT,
                    CHALLANPAIDDATE,
                    PAYMENTREFNO,
                    CHALLANSTATUS,
                    BANKACCNO,
                    BANKNAME,
                    BANKNAME2,
                    FACCODE,
                    DEPCODE,
                    ACADPROGCODE,
                    BATCHCODE,
                    BATCHNAME,
                    ACADPROGNAME,
                    DEPNAME,
                    FACNAME,
                    ENTRYTIME,
                    challandatetime,
                    to_char(to_date(substr(challandatetime, 0, 11), 'DD-MM-YYYY'), 'DD-MON-YYYY') as gendate,
                    to_char(to_date(substr(challandatetime, 12, length(challandatetime)), 'HH24:MI:SS'), 'HH24:MI:SS') as gentime,
                    to_char(to_date(substr(challandatetime, 12, length(challandatetime)), 'HH24:MI:SS'), 'HH24') as genhour,
                    to_char(to_date(substr(challandatetime, 12, length(challandatetime)), 'HH24:MI:SS'), 'MI') as genmin ,
                    to_char(to_date(substr(challandatetime, 12, length(challandatetime)), 'HH24:MI:SS'), 'SS') as gensec
                FROM feechallanpayment
                )
            WHERE semcode = '$semester' $accNo and gendate LIKE '$date'"
            );
    }

    // public function saveMiscChallanTest()
    // {
    //     $payload = json_decode ( request()->getContent() );
    //     dd($payload); exit();
    // }

    public function saveMiscChallan()
    {
        $payload = json_decode ( request()->getContent() );
        $miscinfo = $payload->miscinfo;
        $feeAmount = $payload->amount ?? '';
        $currentSemester = $this->currentSemester();

        $challanAlreadyExists = DB::connection('oracle')->select("
            SELECT
                s.*, p.*, pd.*, fc.*, d.depname, f.facname, p.bankname2 as bk2name, p.bankaccno2 as bkacc
            FROM
                paychallan p, paychallandetail pd, tbl_hstudents s, feecodes fc, tbl_faculty f, tbl_department d
            WHERE
                p.regno = pd.regno
            AND
                p.regno = '$payload->regno'
            AND
                p.SEMCODE = '$currentSemester'
            AND
                p.semcode = pd.semcode
            AND
                p.CHALLANNO = pd.CHALLANNO
            AND
                s.regno = p.regno
            AND
                pd.feecode = fc.feecode
            AND
                fc.feecode = '$miscinfo->feecode'
            AND
                s.depcode = d.depcode
            AND
                s.faccode = f.faccode
         ");

        /**
         * Check if challan already exists
         */
        if(!empty($challanAlreadyExists)) {
            return $challanAlreadyExists;
        }

        $bankaccno =  $miscinfo->femaleaccountno;
        $bank = $miscinfo->femalebank;

        $bankaccno2 =  $miscinfo->femaleaccountno2;
        $bank2 = $miscinfo->femalebank2;

        if ($payload->gender == 'M' || $payload->gender == 'm')  {
            $bankaccno =  $miscinfo->maleaccountno;
            $bank = $miscinfo->malebank;

            $bankaccno2 =  $miscinfo->maleaccountno2;
            $bank2 = $miscinfo->malebank2;
        }

        $challanno = DB::connection('oracle')
            ->select("
                select dcs.PAYCHLLANNO_SEQ.nextval from dual"
            );
        $challanno = $challanno[0]->nextval;

        DB::connection('oracle')
            ->table('paychallan')
            ->insert(
                array(
                    'regno' => $payload->regno,
                    'semcode' => $currentSemester,
                    'challanno' => $challanno,
                    'challandate' => date('Y-M-d'),
                    'challanduedate' => date('Y-M-d', strtotime('+'.$miscinfo->nofdays.' days')),
                    'challanamnt' => $feeAmount ?: $miscinfo->feeamount,
                    'bankaccno' => $bankaccno,
                    'bankaccno2' => $bankaccno2,
                    'challanstatus' => 'new',
                    'challandatetime' => strtoupper(date('d-M-Y H:i:s')),
                    'bankname' => $bank,
                    'bankname2' => $bank2
                )
            );

        DB::connection('oracle')
            ->table('paychallandetail')
            ->insert(
                array(
                    'regno' => $payload->regno,
                    'semcode' => $currentSemester,
                    'challanno' => $challanno,
                    'feecode' => $miscinfo->feecode,
                    'feeamnt' => $feeAmount ?: $miscinfo->feeamount
                )
            );

        $response = DB::connection('oracle')->select("
            SELECT
                s.*, p.*, pd.*, fc.*
            FROM
                paychallan p, paychallandetail pd, tbl_hstudents s, feecodes fc
            WHERE
                p.regno = pd.regno
            AND
                p.regno = '$payload->regno'
            AND
                p.SEMCODE = '$currentSemester'
            AND
                p.semcode = pd.semcode
            AND
                p.CHALLANNO = '$challanno'
            AND
                s.regno = p.regno
            AND
                pd.feecode = fc.feecode
            AND
                fc.feecode = '$miscinfo->feecode'
        ");
        return $response;
    }
}
