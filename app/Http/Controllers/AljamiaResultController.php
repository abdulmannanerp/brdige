<?php

namespace App\Http\Controllers;

use App\AljamiaStudentResult;
use App\AljamiaStudent;
use App\AljamiaPayChallan;
use Illuminate\Support\Facades\DB;

class AljamiaResultController extends Controller
{
	public function get_challan_info($challannumber){
		$response = DB::connection('oracle')->select("
    		SELECT
                s.*, p.*, pd.*, fc.*, d.depname, f.facname
            FROM
                paychallan p, paychallandetail pd, tbl_hstudents s, feecodes fc, tbl_faculty f, tbl_department d
            WHERE
                p.regno = pd.regno
            AND
                p.semcode = pd.semcode
            AND
                p.CHALLANNO = $challannumber
            AND
                s.regno = p.regno
            AND
                pd.feecode = fc.feecode
            AND
                s.depcode = d.depcode
            AND
                s.faccode = f.faccode
        ");
        return $response;
	}
	public function get_student_record_get($cnic){
		 if ($cnic) {
		$student = DB::connection('oracle')->select("
    		select * from TBL_STUDENT,TBL_ACADPROG
where (REGEXP_REPLACE (TBL_STUDENT.nidpassno, '-', '') = '".str_replace('-','',$request->cnic)."')
and TBL_STUDENT.acadprogcode=TBL_ACADPROG.acadprogcode
        ");
		
			if(!$student){
				return 2;
			}else{
				return $student;
			}
		}else{
			 return 0;
		 }
		
		 if ($cnic) {
		$student=AljamiaStudent::query()
            ->where(function($query) use ($cnic) {
                $query->where('nidpassno', $cnic)
				->orWhere('nidpassno', str_replace('-','',$cnic));
            })
            ->first();
			if(!$student){
				return 2;
			}else{
				return $student;
			}
		 }else{
			 return 0;
		 }
	}
	
    public function get_student_record(){
		$request = json_decode(request()->getContent());
		 if ($request && $request->mc_token!='59ed9a9b2870639bad338f096ccf9b999962d3f2'){
			 return 10;
		 }
		 
		 if ($request && $request->cnic) {
			 
		$student = DB::connection('oracle')->select("
    		select * from TBL_STUDENT,TBL_ACADPROG
where (REGEXP_REPLACE (TBL_STUDENT.nidpassno, '-', '') = '".str_replace('-','',$request->cnic)."')
and TBL_STUDENT.acadprogcode=TBL_ACADPROG.acadprogcode AND TBL_STUDENT.conv in(121,122,123,124,125)
        ");
		
				return $student;
			 
		$student=AljamiaStudent::query()
            ->where(function($query) use ($request) {
                $query->where('nidpassno', $request->cnic)
                    ->orWhere('nidpassno', $request->cnicWithoutDashes);
            })
            ->first();
			
				return $student;
			
		 }else{
			 return 0;
		 }
	}
	public function transformResult(){
		 $request = json_decode(request()->getContent());
		 if ($request && $request->mc_token!='59ed9a9b2870639bad338f096ccf9b999962d3f2'){
			 return 10;
		 }
		 if ($request && $request->regno) {
			if(!$this->isStudentExist($request->regno)){
				return 3;
			}
		 }else{
			 return 0;
		 }
		 $studentJoining=AljamiaStudentResult::where('regno', $request->regno)
		 ->where('coursecode',$request->course_code)
		  ->where('semcode',$request->semester)
		 ->get();
		 if($studentJoining->count() > 1){
			 return 4;
		 }else if($studentJoining->count()== 0){
			  return 5;
		 }
		 /*$transformed=AljamiaStudentResult::where('regno', $request->regno)
		 ->where('coursecode',$request->course_code)
		  ->where('semcode',$request->semester)
		  ->update([
					"MARKS" => "".$request->final_marks."",
					"MIDTERM" => "".$request->mid_marks."",
					"POINTS" => "".$request->point."",
					"GRADE" => "".$request->grade."",
				]);*/
				if(!empty($request->feedback)){
					$sql="UPDATE tbl_studsemcourse SET feedback=1 WHERE regno='".$request->regno."' AND coursecode='".$request->course_code."' AND semcode='".$request->semester."'";
				}else{
				$sql="UPDATE TBL_studsemcours SET marks='".$request->final_marks."', midterm='".$request->mid_marks."', points='".$request->point."', grade='".$request->grade."' WHERE regno='".$request->regno."' AND coursecode='".$request->course_code."' AND semcode='".$request->semester."'";
				}
				$transformed=DB::update($sql);
				//return $transformed;
				if($transformed){
					return 1;
				}else{
					return 2;
				}
		 //return "{final_marks: ".$request->final_marks."}"."{midTermMarks: ".$request->mid_marks."}"."{grade: ".$request->grade."}"."{regno: ".$request->regno."}"."{point: ".$request->point."}"."{course_code: ".$request->course_code."}"."{semester: ".$request->semester."}";
	}
    public function isStudentExist($regno)
    {
		 $isStudentExits=AljamiaStudent::where('regno', $regno)->first();
			if($isStudentExits){
				return true;
			}else{
				return false;
			}
		/*
      $request = json_decode(request()->getContent());
      if ($request && $request->regno) {
           
      }
        return; */
    }
}
