<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AljamiaNewStudent extends Model
{
    protected $table = 'newstudent';
    protected $guarded = [];
    protected $primaryKey = null;
    public $incrementing = false;
    public $timestamps = false;
}
