<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AljamiaDepartment extends Model
{
    protected $table = 'TBL_DEPARTMENT';
}
