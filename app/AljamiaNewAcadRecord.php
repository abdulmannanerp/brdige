<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AljamiaNewAcadRecord extends Model
{
    protected $table = 'newacadrecord';
    protected $guarded = [];
    protected $primaryKey = null;
    public $incrementing = false;
    public $timestamps = false;
}
