<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AljamiaPayChallanDetail extends Model
{
    protected $table = 'paychallandetail';


    public function feeCode()
    {
    	return $this->belongsTo('App\AljamiaFeeCodes', 'feecode', 'feecode');
    }
}
