<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AljamiaStudentSemesterCourse extends Model
{
    protected $table = 'tbl_studsemcourse';


    public function student()
    {
    	return $this->belongsTo('App\AljamiaStudent', 'regno', 'regno');
    }

    public function batch()
    {
    	return $this->belongsTo('App\AljamiaBatch', 'batchcode', 'batchcode');
    }
}
