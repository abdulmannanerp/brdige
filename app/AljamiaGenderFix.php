<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AljamiaGenderFix extends Model
{
    protected $table = 'gender_fix';
    protected $guarded = [];
}
