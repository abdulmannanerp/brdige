<?php

use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return 'Silence Is Golden';
})->name('home');
Route::get('/test-ks', 'TestController@index');
Route::post('/aljamia-result-transform', 'AljamiaResultController@transformResult');
Route::post('/aljamia-student-data', 'AljamiaResultController@get_student_record');
Route::get('/aljamia-student-data-get/{cnic}', 'AljamiaResultController@get_student_record_get');
Route::get('/challan/{challannumber}', 'AljamiaResultController@get_challan_info');

/* ********************************************************************************************* */
/* ********************************************************************************************* */
// Get Student Picture from the Bridge Route
/* ********************************************************************************************* */
// Route::post('/aljamia-get-student-picture', 'AljamiaStdPermissionController@getStudentPicture');
/* ********************************************************************************************* */
// Get Student GPA/Remarks/CrHr from the Bridge Route
/* ********************************************************************************************* */
// Route::post('/aljamia-get-student-gpa-remarks-crhr', 'AljamiaStdPermissionController@getStudentGpaRemarksCrHrs');
/* ********************************************************************************************* */
// Get Student CGPA/Overall Percentage from the Bridge Route
/* ********************************************************************************************* */
// Route::post('/aljamia-get-student-cgpa-overallpercentage', 'AljamiaStdPermissionController@getStudentCgpaOverallPercentage');
/* ********************************************************************************************* */
// Get Student Record from the Bridge Route
/* ********************************************************************************************* */
Route::post('/aljamia-get-student-courses', 'AljamiaStdPermissionController@getStudentCourses');

Route::post('/aljamia-get-student-courses-qr', 'AljamiaStdPermissionController@getstudentQrCodeData');

Route::post('/aljamia-get-student-intimation', 'AljamiaStdPermissionController@getStudentIntimation');

Route::post('/aljamia-get-student-permission-form', 'AljamiaStdPermissionController@permissionForm');

Route::post('/aljamia-get-student-paid-amount', 'AljamiaStdPermissionController@paidAmount');
/* ********************************************************************************************* */
/* ********************************************************************************************* */
Route::get('/get-rollnoslip-information', 'GetStudentRollNumberSlip@getslipByRegno');
Route::get('/get-picture-information', 'GetStudentRollNumberSlip@getpicByRegno');
Route::get('/get-faculty-information', 'GetStudentRollNumberSlip@getfaculty');
Route::get('/get-acadprograme-information', 'GetStudentRollNumberSlip@getacadprograme');
Route::get('/get-batch-information', 'GetStudentRollNumberSlip@getbatch');
Route::get('/get-semdate-information', 'GetStudentRollNumberSlip@getsemDate');
Route::get('/is-student-defaulter', 'GetStudentRollNumberSlip@isdefualter');
Route::get('/is-student-prevented', 'GetStudentRollNumberSlip@isPrevented');
Route::get('/is-admission-cancelled', 'GetStudentRollNumberSlip@isAdmissionCancelled');

Route::get('/test-is-student-defaulter', 'GetStudentRollNumberSlip@testisdefualter');
Route::get('/test-is-admission-cancelled', 'GetStudentRollNumberSlip@TestisAdmissionCancelled');
Route::middleware(['revokeUnknownIps'])->group(function(){
	/**
	 * Search students
	 */
	Route::post('/get-student-by-email', 'AljamiaStudentController@findStudentByEmail');

	Route::post('/get-student-by-regno', 'AljamiaStudentController@findStudentByRegistrationNumber');
	Route::post('/get-students-by-batch', 'AljamiaStudentController@getStudentsByBatch');
	Route::post('/get-student-by-name-and-cnic', 'AljamiaStudentController@getStudentByNameAndCnic');
	Route::get('/get-student-by-name-and-cnic-test', 'AljamiaStudentController@getStudentByNameAndCnictest');
	
	/**
	 * Get student current challan information by regno
	 */
	Route::post('/get-challan-information', 'GetChallanInformationFromDb@index'); 

	// Route to get QR and save it
	Route::post('/get-qr-information', 'GetChallanInformationFromDb@qrCode');


	Route::post('/get-challans', 'ChallanVerificationController@index');
	Route::post('/verify-challans', 'ChallanVerificationController@verifyChallans');
	Route::post('/save-misc-challan', 'GetChallanInformationFromDb@saveMiscChallan');
	// Route::post('/save-misc-challan-test', 'GetChallanInformationFromDb@saveMiscChallanTest');

	/**
	 * Returns challan information which is then sent to Allied bank
	 */
	Route::get('/bulid-challan-csv-abl', 'GetChallanInformationFromDb@getChallansGeneratedInPastHour');
	/* *********************************************************************************** */
	//							   FAYSAL BANK BRIDGE ROUTE
	/* *********************************************************************************** */
	Route::get('/bulid-challan-csv-faysal', 'GetChallanInformationFromDb@getChallansGeneratedInPastHourFaysal');
	/* *********************************************************************************** */
	//										END
	/* *********************************************************************************** */
	/* *********************************************************************************** */
	//							   Bank alfalah  ROUTE
	/* *********************************************************************************** */
	Route::get('/bulid-challan-csv-alfalah', 'GetChallanInformationFromDb@getChallansGeneratedInPastHourAlfalah');
	/* *********************************************************************************** */
	//										END
	/* *********************************************************************************** */
	Route::get('/bulid-challan-csv-abl-fwbl', 'GetChallanInformationFromDb@getChallansGeneratedInPastHourFwbl');
	Route::post('/get-challans-by-date', 'GetChallanInformationFromDb@getChallanByDate');

	Route::post('/search', 'SearchController@index');

	/**
	 * Utilities
	 */
	Route::get('/fee-codes', 'AljamiaUtilityController@feeCodes');
	Route::get('/tab', 'AljamiaUtilityController@getTables');
	Route::get('/faculty', 'AljamiaUtilityController@faculty');
	Route::get('/department', 'AljamiaUtilityController@department');
	Route::get('/program', 'AljamiaUtilityController@program');
	Route::get('/batch', 'AljamiaUtilityController@batch');
	Route::get('/semester', 'AljamiaUtilityController@semester');

	Route::post('/migrate', 'MigrationController@migrate');
	Route::post('/update-gender', 'MigrationController@updateGender');
	/**
	 * Get student joining
	 */
	Route::post('/joining', 'StudentSemesterCourseController@index');
}); //end middleware
